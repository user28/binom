<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpressionCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expression_company', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('expression_id')->unsigned()->default(1);
            $table->bigInteger('rule_id')->unsigned()->default(1);
            $table->bigInteger('company_id')->unsigned()->default(1);

            $table->foreign('expression_id')->references('id')->on('expression');
            $table->foreign('rule_id')->references('id')->on('rule');
            $table->foreign('company_id')->references('id')->on('company');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expression_company');
    }
}
