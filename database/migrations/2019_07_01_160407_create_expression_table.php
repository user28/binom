<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpressionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expression', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('action_id')->unsigned()->default(1);
            $table->bigInteger('entity_id')->unsigned()->default(1);
            $table->bigInteger('date_id')->unsigned()->default(1);


            $table->foreign('action_id')->references('id')->on('action');
            $table->foreign('entity_id')->references('id')->on('entity');
            $table->foreign('date_id')->references('id')->on('date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expression');
    }
}
